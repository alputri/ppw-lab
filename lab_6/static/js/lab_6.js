// chat box

var displayChat = document.getElementById('msg');

if (!localStorage.getItem("chat")) {
    localStorage.setItem("chat", JSON.stringify([]));
}

var chatHistory = JSON.parse(localStorage.getItem("chat"));


var send = document.getElementById('enter');
var i = 0;

send.addEventListener('keypress', function (e) {
    var key = e.which || e.keyCode;
    if (key === 13) {
        if (document.getElementById('enter').value === "") {
            alert("Please insert message")
            return
        }

        else if (document.getElementById('enter').value === "/clear") {
            displayChat.innerHTML = '';
            document.getElementById('enter').value = "";
            localStorage.removeItem("chat");
            return
        }

        var message = {
            text: "Alya : " + document.getElementById('enter').value,
            dateTime: new Date().toLocaleTimeString() + " " + new Date().toDateString(),
        };

        document.getElementById('enter').value = "";
        chatHistory.push(message);
        localStorage.setItem("chat", JSON.stringify(chatHistory));

        localData = localStorage.getItem("chat");
        localData = JSON.parse(localData);

        var templateDiv =
            "<div class='message'>" +
            "<p class='text'>" + localData[localData.length - 1].text + "</p>" + "<div class='clear'></div>" + "</div>" +
            "<p class='datetime'>" + localData[localData.length - 1].dateTime + "</p>" + "<div class='clear'></div>";

        displayChat.innerHTML += templateDiv;
    }

});

function refresh() {
    localData = localStorage.getItem("chat");
    localData = JSON.parse(localData);
    for (var i = 0; i < localData.length; i++) {
        var templateDiv =
            "<div class='message'>" +
            "<p class='text'>" + localData[i].text + "</p>" + "<div class='clear'></div>" + "</div>" +
            "<p class='datetime'>" + localData[i].dateTime + "</p>" + "<div class='clear'></div>";
        displayChat.innerHTML += templateDiv;
    }
}

$(document).ready(function(){
	console.log("hey");
	refresh();
    $(".chat-head").click(function(){
        $(".chat-body").toggle(500);
    });
});
//END

// Calculator
var print = document.getElementById('print');
var erase = false;

Math.radians = function (degrees) {
    return degrees * Math.PI / 180;
};

Math.degrees = function (radians) {
    return radians * 180 / Math.PI;
};


var go = function (x) {
    if (x === 'ac') {
        print.value = null;
        erase = false;
    } else if (x === 'eval') {
        print.value = (evil(print.value));
        erase = true;
    } else if (x === 'sin' || x === 'tan') {
        print.value = (evil('Math.' + x + '(Math.radians(' + evil(print.value) + '))'));
        erase = true;
    } else if (x === 'log') {
        print.value = ((Math.log10(evil(print.value))));
        erase = true;
    } else if (erase === true) {
        print.value = x;
        erase = false;
    } else {
        print.value += x;
    }
};

function evil(fn) {
    return new Function('return ' + fn)();
}

// END

// Select2
function changeTheme(x) {
    $('body').css({"backgroundColor": x['bcgColor']});
    $('.text-center').css({"color": x['fontColor']});
    $('.calculator .model').css({"background" : x['calcColor']});

    $('.chat-head').css({"background" : x['calcColor']});
    $('.chat-head h2').css({"color" : x['fontColor']});

}

localStorage.setItem('themes', '[' +
                                    '{"id":0,"text":"Red","bcgColor":"#800020","fontColor":"#F98787","calcColor":"#560019"},' +
                                    '{"id":1,"text":"Pink","bcgColor":"#ED7A9B","fontColor":"#7F294C","calcColor":"#EAB0C3"},' +
                                    '{"id":2,"text":"Purple","bcgColor":"#3B3B6D","fontColor":"#9D9DE0","calcColor":"#2A2A4C"},' +
                                    '{"id":3,"text":"Indigo","bcgColor":"#00416A","fontColor":"#87CEEB","calcColor":"#023149"},' +
                                    '{"id":4,"text":"Blue","bcgColor":"#87CEEB","fontColor":"#00416A","calcColor":"#A6EAFF"},' +
                                    '{"id":5,"text":"Teal","bcgColor":"#008080","fontColor":"#69F7F0","calcColor":"#005E5B"},' +
                                    '{"id":6,"text":"Lime","bcgColor":"#B1E200","fontColor":"#55660E","calcColor":"#F0FFB0"},' +
                                    '{"id":7,"text":"Yellow","bcgColor":"#FFD300","fontColor":"#7C5300","calcColor":"#FFEEA9"},' +
                                    '{"id":8,"text":"Amber","bcgColor":"#FFBF00","fontColor":"#6B4700","calcColor":"#FFDF8F"},' +
                                    '{"id":9,"text":"Orange","bcgColor":"#F79239","fontColor":"#7A451E","calcColor":"#FFB883"},' +
                                    '{"id":10,"text":"Brown","bcgColor":"#7A451E","fontColor":"#EA9357","calcColor":"#5E2D0D"}]');

var themes = JSON.parse(localStorage.getItem('themes'));
if (localStorage.getItem('selectedTheme') === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(themes[3])); //default
}

var theme = JSON.parse(localStorage.getItem('selectedTheme'));
changeTheme(theme);

//toggle
$(document).ready(function() {
    $('.my-select').select2({'data': themes}).val(theme['id']).change();
    $('.my-select').on('change', function () {
        theme = themes[$('.my-select').val()];
        changeTheme(theme);
    })
});

//set
$(document).ready(function() {
    $('.my-select').select2({'data': themes}).val(theme['id']).change();
    $('.apply-button').on('click', function () {
        theme = themes[$('.my-select').val()];
        changeTheme(theme);
        localStorage.setItem('selectedTheme', JSON.stringify(theme));
    })
});

// END