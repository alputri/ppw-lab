from django.shortcuts import render

# Create your views here.
def index(request):
    html = 'portfolio.html'
    return render(request,html)